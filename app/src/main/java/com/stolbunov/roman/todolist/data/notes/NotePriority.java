package com.stolbunov.roman.todolist.data.notes;

public enum NotePriority {
    HIGH, MEDIUM, LOW
}
