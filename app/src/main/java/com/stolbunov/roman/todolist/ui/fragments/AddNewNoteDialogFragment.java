package com.stolbunov.roman.todolist.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.stolbunov.roman.todolist.R;
import com.stolbunov.roman.todolist.data.notes.Note;
import com.stolbunov.roman.todolist.data.notes.NotePriority;

public class AddNewNoteDialogFragment extends DialogFragment implements View.OnClickListener {
    private AppCompatEditText title;
    private AppCompatEditText description;
    private AppCompatSpinner selectPriority;
    private OnSaveDataClickListener listener;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnSaveDataClickListener) {
            listener = (OnSaveDataClickListener) context;
//        } else {
//            throw new IllegalStateException("Implement OnSaveDataClickListener");
//        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OnSaveDataClickListener) {
            listener = (OnSaveDataClickListener) activity;
        } else {
            throw new IllegalStateException("Implement OnSaveDataClickListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public static AddNewNoteDialogFragment getInstance() {
        return new AddNewNoteDialogFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dialog_add_new_note, container, false);
        initChildrenView(view);
        setTransparentBackground();
        return view;
    }

    private void setTransparentBackground() {
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
    }

    private void initChildrenView(View view) {
        title = view.findViewById(R.id.fd_new_note_title);
        description = view.findViewById(R.id.fd_new_note_description);
        selectPriority = view.findViewById(R.id.fd_new_note_select_priority);
        AppCompatButton cancel = view.findViewById(R.id.fd_new_note_cancel);
        AppCompatButton save = view.findViewById(R.id.fd_new_note_save);

        cancel.setOnClickListener(this);
        save.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fd_new_note_cancel:
                dismiss();
                break;
            case R.id.fd_new_note_save:
                if (checkInputData()) {
                    listener.onSaveClick(createNewNote());
                    dismiss();
                }
                break;
        }
    }

    private Note createNewNote() {
        return new Note(
                title.getText().toString(),
                description.getText().toString(),
                NotePriority.valueOf(selectPriority.getSelectedItem().toString()));
    }

    private boolean checkInputData() {
        if (listener == null) {
            return false;
        }

        if (TextUtils.isEmpty(title.getText())) {
            listener.onErrorSaveClick(getString(R.string.fd_hint_text_title));
            return false;
        }

        if (TextUtils.isEmpty(description.getText())) {
            listener.onErrorSaveClick(getString(R.string.fd_hint_text_description));
            return false;
        }
        return true;
    }

    public interface OnSaveDataClickListener {
        void onSaveClick(Note note);

        void onErrorSaveClick(String message);
    }

}
