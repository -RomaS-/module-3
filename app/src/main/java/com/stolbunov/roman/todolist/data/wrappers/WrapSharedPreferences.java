package com.stolbunov.roman.todolist.data.wrappers;

import android.content.Context;
import android.content.SharedPreferences;

import com.stolbunov.roman.todolist.data.notes.Note;
import com.stolbunov.roman.todolist.data.wrappers.threads.NotesLoading;

import java.util.List;

public class WrapSharedPreferences implements NotesLoading.LoadingCompleteCallback {
    private static final String KEY_NUMBER_NOTES = "number notes";
    private static final String KEY_TITLE = "title";
    private static final String KEY_DESCRIPTION = "description";
    private static final String KEY_PRIORITY = "priority";
    private static long counter = 0;
    private SharedPreferences preferences;
    private FinishLoadingCallback callback;

    public WrapSharedPreferences(Context context, String namePreferences) {
        preferences = context.getSharedPreferences(namePreferences, Context.MODE_PRIVATE);
    }

    public void add(Note note) {
        preferences.edit()
                .putLong(KEY_NUMBER_NOTES, counter)
                .putString(KEY_TITLE + counter, note.getTitle())
                .putString(KEY_DESCRIPTION + counter, note.getDescription())
                .putString(KEY_PRIORITY + counter, note.getPriority().name())
                .apply();
        counter++;
    }

    public void loadListNotes(FinishLoadingCallback callback) {
        this.callback = callback;
        long count = preferences.getLong(KEY_NUMBER_NOTES, -1);

        if (count < 0) {
            counter = 0;
        } else {
            counter = count + 1;
            NotesLoading loading = new NotesLoading(counter, preferences, this);
            loading.execute(KEY_TITLE, KEY_DESCRIPTION, KEY_PRIORITY);
        }
    }

    @Override
    public void complete(List<Note> notes) {
        if (callback != null) {
            callback.onComplete(notes);
        }
    }

    public void remove(Note note) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(KEY_TITLE + note.getId());
        editor.remove(KEY_DESCRIPTION + note.getId());
        editor.remove(KEY_PRIORITY + note.getId());
        editor.apply();
    }

    public void itemChanged(Note note) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_TITLE + note.getId(), note.getTitle());
        editor.putString(KEY_DESCRIPTION + note.getId(), note.getDescription());
        editor.putString(KEY_PRIORITY + note.getId(), note.getPriority().name());
        editor.apply();
    }

    public interface FinishLoadingCallback {
        void onComplete(List<Note> notes);
    }

}
