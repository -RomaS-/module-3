package com.stolbunov.roman.todolist.ui.screens;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.stolbunov.roman.todolist.R;
import com.stolbunov.roman.todolist.data.notes.Note;
import com.stolbunov.roman.todolist.data.wrappers.WrapSharedPreferences;
import com.stolbunov.roman.todolist.ui.adapters.NoteAdapter;
import com.stolbunov.roman.todolist.ui.adapters.OnItemClickListener;
import com.stolbunov.roman.todolist.ui.fragments.AddNewNoteDialogFragment;

import java.util.List;

public class MainActivity extends AppCompatActivity implements
        AddNewNoteDialogFragment.OnSaveDataClickListener,
        WrapSharedPreferences.FinishLoadingCallback, OnItemClickListener,
        NoteAdapter.OnRemoveNoteListener {

    public final static int RC_NOTE_EDIT = 351;
    private final String NOTE_PREFERENCES = "notes";
    private WrapSharedPreferences preferences;
    private NoteAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.main_toolbar);
        toolbar.setTitle(R.string.title_toolbar);
        setSupportActionBar(toolbar);

        preferences = new WrapSharedPreferences(this, NOTE_PREFERENCES);
        preferences.loadListNotes(this);

        initFAB();
        initNoteAdapter();
        initNoteRecyclerView(getManager());
    }

    private void initNoteAdapter() {
        adapter = new NoteAdapter();
        adapter.setItemClickListener(this);
        adapter.setRemoveListener(this);
    }

    private void initNoteRecyclerView(RecyclerView.LayoutManager manager) {
        RecyclerView recyclerView = findViewById(R.id.rv_notes);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(manager);
    }

    @NonNull
    private LinearLayoutManager getManager() {
        return new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
    }

    private void initFAB() {
        FloatingActionButton button = findViewById(R.id.fab);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddNewNoteDialogFragment dialogFragment = AddNewNoteDialogFragment.getInstance();
                dialogFragment.show(getSupportFragmentManager(), "AddNewNoteDialogFragment");
            }
        });
    }

    @Override
    public void onSaveClick(Note note) {
        preferences.add(note);
        adapter.add(note);
    }

    @Override
    public void onErrorSaveClick(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onComplete(List<Note> notes) {
        adapter.setData(notes);
    }

    @Override
    public void onRemoveNote(Note note, int position) {
        adapter.remove(position);
        preferences.remove(note);
    }

    @Override
    public void onItemClick(Note note, int position) {
        Intent intent = new Intent(this, NoteEditorActivity.class);
        intent.putExtra(NoteEditorActivity.KEY_INTENT_NOTE, note);
        startActivityForResult(intent, RC_NOTE_EDIT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case RC_NOTE_EDIT:
                    if (data != null) {
                        Note note = data.getParcelableExtra(NoteEditorActivity.KEY_INTENT_NOTE);

                        adapter.add(note);
                        preferences.itemChanged(note);
                    }
                    break;
            }
        }
    }


}
