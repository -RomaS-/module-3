package com.stolbunov.roman.todolist.ui.widgets;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import com.stolbunov.roman.todolist.R;

import java.util.Set;

public class CustomPriorityView extends FrameLayout {
    private AppCompatImageView centerImage;
    private AppCompatImageView indicatorImage;

    public CustomPriorityView(@NonNull Context context) {
        this(context, null);
    }

    public CustomPriorityView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomPriorityView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.widget_priority_note, this, true);
        centerImage = findViewById(R.id.custom_view_center_image);
        indicatorImage = findViewById(R.id.custom_view_indicator_image);
    }

    public void setCenterImageResource(@DrawableRes int resId) {
        centerImage.setImageResource(resId);
    }

    public void setBackgroundTintList(ColorStateList tint) {
        indicatorImage.setBackgroundTintList(tint);
    }
}
