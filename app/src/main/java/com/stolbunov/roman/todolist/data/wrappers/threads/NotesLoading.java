package com.stolbunov.roman.todolist.data.wrappers.threads;

import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.stolbunov.roman.todolist.data.notes.Note;
import com.stolbunov.roman.todolist.data.notes.NotePriority;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class NotesLoading extends AsyncTask<String, Void, List<Note>> {
    private long numberNotes;
    private SharedPreferences preferences;
    private LoadingCompleteCallback callback;

    public NotesLoading(long numberNotes, SharedPreferences preferences, LoadingCompleteCallback callback) {
        this.preferences = preferences;
        this.numberNotes = numberNotes;
        this.callback = callback;
    }

    @Override
    protected List<Note> doInBackground(String... strings) {
        List<Note> notes = new LinkedList<>();
        for (int i = 0; i < numberNotes; i++) {
            String title = preferences.getString(strings[0] + i, strings[0]);
            String description = preferences.getString(strings[1] + i, strings[1]);
            String priority = preferences.getString(strings[2] + i, strings[2]);

            if (!priority.equals(strings[2])) {
                notes.add(new Note(i, title, description, NotePriority.valueOf(priority)));
            }
        }
        Collections.sort(notes);
        return notes;
    }

    @Override
    protected void onPostExecute(List<Note> notes) {
        super.onPostExecute(notes);
        callback.complete(notes);
    }

    public interface LoadingCompleteCallback {
        void complete(List<Note> notes);
    }
}
