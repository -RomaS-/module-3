package com.stolbunov.roman.todolist.ui.adapters;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.stolbunov.roman.todolist.R;
import com.stolbunov.roman.todolist.data.notes.Note;
import com.stolbunov.roman.todolist.ui.widgets.CustomPriorityView;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.NoteViewHolder> {
    private Set<Note> data;
    private OnRemoveNoteListener removeListener;
    private OnItemClickListener itemClickListener;

    public NoteAdapter() {
        data = new TreeSet<>();
    }

    public NoteAdapter(TreeSet<Note> data) {
        this.data = data;

    }

    @NonNull
    @Override
    public NoteViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int itemType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.item_main_notes, viewGroup, false);
        return new NoteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NoteViewHolder noteViewHolder, int position) {
        noteViewHolder.bind(getNoteByPosition(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<Note> data) {
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    public void add(Note note) {
        if (data.add(note)) {
            notifyItemInserted(getIndex(note));
        }
    }

    private Note getNoteByPosition(int position) {
        int counter = 0;
        for (Note note : data) {
            if (counter == position) {
                return note;
            }
            counter++;
        }
        return null;
    }

    public void remove(int position) {
        removeByPosition(position);
        notifyItemRemoved(position);
    }

    private void removeByPosition(int position) {
        Iterator<Note> iterator = data.iterator();
        for (int i = 0; iterator.hasNext(); i++) {
            iterator.next();
            if (i == position) {
                iterator.remove();
                return;
            }
        }
    }

    private int getIndex(Note note) {
        Iterator<Note> iterator = data.iterator();
        for (int i = 0; iterator.hasNext(); i++) {
            if (note.equals(iterator.next())) {
                return i;
            }
        }
        return -1;
    }

    public void setRemoveListener(OnRemoveNoteListener removeListener) {
        this.removeListener = removeListener;
    }

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    class NoteViewHolder extends RecyclerView.ViewHolder {
        AppCompatTextView title;
        AppCompatTextView description;
        AppCompatImageButton remove;
        CustomPriorityView priority;

        public NoteViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.rv_note_title);
            description = itemView.findViewById(R.id.rv_note_description);
            remove = itemView.findViewById(R.id.rv_note_remove);
            priority = itemView.findViewById(R.id.rv_note_priority);
        }

        public void bind(Note note) {
            remove.setOnClickListener(v -> onRemoveClick(note, getAdapterPosition()));
            itemView.setOnClickListener(v -> onItemClick(note, getAdapterPosition()));

            title.setText(note.getTitle());
            description.setText(note.getDescription());
            fillingPriority(note);
        }

        private void fillingPriority(Note note) {
            switch (note.getPriority()) {
                case HIGH:
                    priority.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
                    priority.setCenterImageResource(R.drawable.ic_priority_high);
                    break;
                case MEDIUM:
                    priority.setBackgroundTintList(ColorStateList.valueOf(Color.YELLOW));
                    priority.setCenterImageResource(R.drawable.ic_priority_medium);
                    break;
                case LOW:
                    priority.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
                    priority.setCenterImageResource(R.drawable.ic_priority_low);
                    break;
            }
        }

        private void onItemClick(Note note, int position) {
            if (itemClickListener != null) {
                itemClickListener.onItemClick(note, position);
                remove(position);
            }
        }

        private void onRemoveClick(Note note, int position) {
            if (removeListener != null) {
                removeListener.onRemoveNote(note, position);
            }
        }
    }

    public interface OnRemoveNoteListener {
        void onRemoveNote(Note note, int position);
    }
}
