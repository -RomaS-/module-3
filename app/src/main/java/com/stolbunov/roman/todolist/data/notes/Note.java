package com.stolbunov.roman.todolist.data.notes;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import java.util.Objects;

public class Note implements Comparable<Note>, Parcelable {
    private long id;
    private String title;
    private String description;
    private NotePriority priority;

    public Note(String title, String description, NotePriority priority) {
        this(0, title, description, priority);
    }

    public Note(long id, String title, String description, NotePriority priority) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.priority = priority;
    }

    protected Note(Parcel in) {
        id = in.readLong();
        title = in.readString();
        description = in.readString();
        priority = NotePriority.valueOf(in.readString());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(priority.name());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Note> CREATOR = new Creator<Note>() {
        @Override
        public Note createFromParcel(Parcel in) {
            return new Note(in);
        }

        @Override
        public Note[] newArray(int size) {
            return new Note[size];
        }
    };

    @Override
    public int compareTo(@NonNull Note note) {
        int result;
        result = Integer.compare(priority.ordinal(), note.getPriority().ordinal());
        if (result != 0) {
            return result;
        }
        result = Long.compare(id, note.getId());
        if (result != 0) {
            return result;
        }
        return Integer.compare(description.length(), note.getDescription().length());
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public NotePriority getPriority() {
        return priority;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPriority(NotePriority priority) {
        this.priority = priority;
    }

    public long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Note)) return false;
        Note note = (Note) o;
        return id == note.id &&
                Objects.equals(title, note.title) &&
                Objects.equals(description, note.description) &&
                priority == note.priority;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, description, priority);
    }
}
