package com.stolbunov.roman.todolist.ui.adapters;

import com.stolbunov.roman.todolist.data.notes.Note;

public interface OnItemClickListener {
    void onItemClick(Note note, int position);
}
