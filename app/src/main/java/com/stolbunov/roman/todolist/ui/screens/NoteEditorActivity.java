package com.stolbunov.roman.todolist.ui.screens;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.stolbunov.roman.todolist.R;
import com.stolbunov.roman.todolist.data.notes.Note;
import com.stolbunov.roman.todolist.data.notes.NotePriority;

public class NoteEditorActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String KEY_INTENT_NOTE = "KEY_INTENT_NOTE";

    private AppCompatEditText title;
    private AppCompatEditText description;
    private AppCompatTextView showPriority;

    private Note note;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_editor);
        Toolbar toolbar = findViewById(R.id.main_toolbar);
        toolbar.setTitle(R.string.note_editor);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        note = intent.getParcelableExtra(KEY_INTENT_NOTE);

        initFAB();
        initView();
        filingFields(note);
    }

    private void initFAB() {
        FloatingActionButton button = findViewById(R.id.note_editor_fab);
        button.setOnClickListener(this);
    }

    private void filingFields(Note note) {
        title.setText(note.getTitle());
        description.setText(note.getDescription());
        showPriority.setText(note.getPriority().name());
    }

    private void initView() {
        title = findViewById(R.id.note_editor_title);
        description = findViewById(R.id.note_editor_description);
        showPriority = findViewById(R.id.note_editor_show_priority);
        AppCompatButton highPriority = findViewById(R.id.note_editor_high_priority);
        AppCompatButton mediumPriority = findViewById(R.id.note_editor_medium_priority);
        AppCompatButton lowPriority = findViewById(R.id.note_editor_low_priority);

        highPriority.setOnClickListener(this);
        mediumPriority.setOnClickListener(this);
        lowPriority.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.note_editor_high_priority:
                showPriority.setText(R.string.high);
                break;
            case R.id.note_editor_medium_priority:
                showPriority.setText(R.string.medium);
                break;
            case R.id.note_editor_low_priority:
                showPriority.setText(R.string.low);
                break;
            case R.id.note_editor_fab:
                createNewNote();
                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra(KEY_INTENT_NOTE, note);
                setResult(RESULT_OK, intent);
                finish();
                break;
        }
    }

    private void createNewNote() {
        note.setTitle(title.getText().toString());
        note.setDescription(description.getText().toString());
        note.setPriority(NotePriority.valueOf(showPriority.getText().toString()));
    }
}
